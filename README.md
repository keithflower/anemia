# anemia

Example use of a forward-chaining rules engine for clinical decision support.

## Overview

See <https://keithflower.org/anemia-diagnosis-with-a-forward-chaining-rules-engine/> for a discussion. 

See <https://keithflower.org/anemia/index.html> for a demo.

## Development

To get an interactive development environment run:

    clojure -A:fig:build

## License

Copyright © 2021 Keith Flower.

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
