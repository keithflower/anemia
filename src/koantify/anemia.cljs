(ns ^:figwheel-hooks koantify.anemia
  (:require
   [reagent.core :as r]
   [goog.object :as gobj]
   ["@material-ui/core" :as mui]
   ["@material-ui/core/styles" :refer [createMuiTheme withStyles]]
   ["@material-ui/core/colors" :as mui-colors]
   [reagent.impl.template :as rtpl]
   [cljss.core :as css :refer-macros [defstyles]]
   [goog.dom :as gdom]
   [koantify.rules :as rules]))

(def custom-theme
  (createMuiTheme
   #js {:palette #js
        {:primary #js
         {:main (gobj/get (.-blue ^js/Mui.Colors mui-colors) 100)}}}))

(defstyles textboxStyles []
  {:margin "5px 5px !important"
   :width "30ch"
   ;; The following class is a child div of textfield div
   ;; (MuiTextField-root) - found via browser element inspection
   ".MuiOutlinedInput-root" {:border-radius "20px"}})

(defn checkboxes []
  [:> mui/Box
  [:> mui/FormControlLabel {:control (r/as-element [:> mui/Checkbox {:id "sideroblasts"}]) :label "Sideroblasts"}]
  [:> mui/FormControlLabel {:control (r/as-element [:> mui/Checkbox {:id "siderocytes"}]) :label "Siderocytes"}]
  [:> mui/FormControlLabel {:control (r/as-element [:> mui/Checkbox {:id "spherocytes"}]) :label "Spherocytes"}] 
   [:br]
   [:> mui/FormControlLabel {:control (r/as-element [:> mui/Checkbox {:id "teardrops"}]) :label "Teardrop RBC"}]
   [:> mui/FormControlLabel {:control (r/as-element [:> mui/Checkbox {:id "targetcells"}]) :label "Target cells"}]
   [:br]
  [:> mui/FormControlLabel {:control (r/as-element [:> mui/Checkbox {:id "splenomegaly"}]) :label "Splenomegaly"}]])

(def lab-names ["hct" "hgb" "rbc" "mcv" "mchc" "rdw" "retic" "plt" "wbc" "fe" "tibc" "ferritin" "b12" "folate" "methylmalonate" "homocysteine"])

(def checkbox-names ["spherocytes" "siderocytes" "sideroblasts" "teardrops" "targetcells" "splenomegaly"])

(defn insert-facts-from-checkboxes [lab-name]
  (let [lab-value (. (.getElementById js/document lab-name) -checked)]
    (println lab-name lab-value)
    (if lab-value (rules/insert-fact lab-name "present"))))

(defn insert-facts-from-values [lab-name]
  (let [lab-value (. (.getElementById js/document lab-name) -value)]
    (if (not= lab-value "")
      (rules/insert-fact lab-name (js/parseFloat lab-value)))))

(def show-results (r/atom false))
(def results (r/atom "Diagnoses:"))

(defn dx []
  (rules/clear-session)
  (dorun (map insert-facts-from-values lab-names))
  (dorun (map insert-facts-from-checkboxes checkbox-names))
  (rules/show-results)
  (reset! rules/results (concat @rules/diagnoses @rules/labs))
  (reset! show-results true))

(defn dx-button []
  [:> mui/Button {:variant "contained" :onClick dx :color "primary"} "Diagnose/advise"])

(defn new-data []
  (reset! show-results false))

(defn textField [id label]
  [:> mui/TextField {:id id :label label :onChange new-data :variant "outlined" :class (textboxStyles)}])

(defn layout []
  [:> mui/MuiThemeProvider {:theme custom-theme}
   [:> mui/CssBaseline
    [:h1 "Anemia diagnosis using a rules engine"]
    (textField "hct" "Hct")
    (textField "hgb" "Hgb")
    (textField "rbc" "RBC count")
    [:br]
    (textField "mcv" "Mean corpuscular volume")
    (textField "mchc" "MCHC")
    (textField "rdw" "RDW")
    [:br]
    (textField "retic" "Reticulocyte count")
    (textField "plt" "Platelet count")
    (textField "wbc" "White blood cell count")
    [:br]
    (textField "fe" "Iron (FE)")
    (textField "tibc" "Total Iron Binding Capacity")
    (textField "ferritin" "Ferritin")
    [:br]
    (textField "b12" "Vit B12")
    (textField "folate" "Folate")
    [:br]
    (textField "methylmalonate" "Methylmalonate")
    (textField "homocysteine" "Homocysteine")
    (checkboxes)
    (dx-button)
    [:br]
    [:br]
    [:> mui/Fade {:in @show-results :timeout 3000}
     [:> mui/Paper
      [:> mui/Typography {:variant "h6"} "Results:"]
      [:pre {:style {:fontFamily "inherit"}} @rules/results]]]]])

  (defn main []
    layout)

(.addEventListener
  js/window
  "DOMContentLoaded"
  (fn []
  (r/render [main] (.getElementById js/document "app"))
    (.log js/console "DOMContentLoaded callback")))

;; specify reload hook with ^;after-load metadata
;; (defn ^:after-load on-reload []
;;     (r/render [main] (.getElementById js/document "app"))
;;   ;; optionally touch your app-state to force rerendering depending on
;;   ;; your application
;;   ;; (swap! app-state update-in [:__figwheel_counter] inc)
;; )
