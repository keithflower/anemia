(ns ^:figwheel-hooks koantify.rules
  (:require-macros [clara.macros :refer [clear-ns-productions!
                                         defrule
                                         defquery
                                         defsession]])
  (:require [reagent.core :as r]
            [clara.rules :refer [insert
                                 fire-rules
                                 query
                                 insert!]]))

(defrecord Lab [test-name value units pt-name])
(defrecord Diagnoses [pt-name diagnosis])

;; Lab normal range limits - the following values
;; are simplified and somewhat arbitrary. Normal
;; hematologic parameters vary based on gender,
;; medical history, etc.
(def hgb-nl            {:low 13   :high 17})
(def hct-nl            {:low 35   :high 50})
(def rbc-count-nl      {:low 3.8  :high 5.7})
(def mcv-nl            {:low 82.5 :high 98})
(def mchc-nl           {:low 32.5 :high 35.2})
(def rdw-nl            {:low 11.4 :high 13.5})
(def retic-count-nl    {:low 16   :high 100})
(def plt-count-nl      {:low 152  :high 330})
(def wbc-count-nl      {:low 3.8  :high 10.4})
(def fe-nl             {:low 60   :high 179})
(def tibc-nl           {:low 240  :high 450})
(def ferritin-nl       {:low 204  :high 360})
(def b12-nl            {:low 160  :high 950})
(def folate-nl         {:low 2.7  :high 17.0})
(def methylmalonate-nl {:low 0.0  :high 0.40})
(def homocysteine-nl   {:low 0.0  :high 15})

(defn lab-normal? [lab val]
    (and (>= val (:low lab))
         (<= val (:high lab))))

(defn lab-low? [lab val]
    (and (< val (:low lab))))

(defn lab-high? [lab val]
    (and (> val (:high lab))))

;; Provide rules for diagnoses, based loosely on:
;;
;; Leung LL et al. UpToDate. "Approach to the adult with anemia." Waltham, MA: UpToDate Inc.
;; https://www.uptodate.com/contents/diagnostic-approach-to-anemia-in-adults (Accessed on February 15, 2021.)

(defrule anemic
  [:or
   [Lab (= test-name 'hct) (lab-low? hct-nl value)]
   [Lab (= test-name 'hgb) (lab-low? hgb-nl value)]   
   [Lab (= test-name 'rbc-count) (lab-low? rbc-count-nl value)]]
  =>
  (insert! (->Diagnoses "pt-0" "anemia")))

(defrule anemic-but-no-indices
  [Diagnoses (= diagnosis "anemia")]
  [:not [Lab (= test-name 'mcv)]]
  =>
  (insert! (->Diagnoses "pt-0" "Advise evaluating RBC indices.")))

(defrule microcytic-anemia
  [Diagnoses (= diagnosis "anemia")]
  [Lab (= test-name 'mcv) (lab-low? mcv-nl value)]
  =>
  (insert! (->Diagnoses "pt-0" "microcytic anemia")))

(defrule microcytic-but-no-iron-studies
  [Diagnoses (= diagnosis "microcytic anemia")]
  [:not [Lab (= test-name 'fe)]]
  [:not [Lab (= test-name 'tibc)]]
  [:not [Lab (= test-name 'ferritin)]]
  =>
  (insert! (->Diagnoses "pt-0" "Advise getting iron studies.")))

(defrule macrocytic-anemia
  [Diagnoses (= diagnosis "anemia")]
  [Lab (= test-name 'mcv) (lab-high? mcv-nl value)]
  =>
  (insert! (->Diagnoses "pt-0" "macrocytic anemia")))

(defrule macrocytic-but-no-workup
  [Diagnoses (= diagnosis "macrocytic anemia")]
  [:not [Lab (= test-name 'b12)]]
  [:not [Lab (= test-name 'folate)]]
  =>
  (insert! (->Diagnoses "pt-0" "Advise obtaining vitamin B12 and folate levels.")))

(defrule b12-deficiency-anemia
  [Diagnoses (= diagnosis "macrocytic anemia")]
  [Lab (= test-name 'b12) (lab-low? b12-nl value)]
  [Lab (= test-name 'methylmalonate) (lab-low? methylmalonate-nl value)]
  [Lab (= test-name 'homocysteine) (lab-high? homocysteine-nl value)]
  =>
  (insert! (->Diagnoses "pt-0" "B12 deficiency anemia")))

(defrule folate-deficiency-anemia
  [Diagnoses (= diagnosis "macrocytic anemia")]
  [Lab (= test-name 'folate) (lab-low? folate-nl value)]
  [Lab (= test-name 'methylmalonate) (lab-normal? methylmalonate-nl value)]
  [Lab (= test-name 'homocysteine) (lab-high? homocysteine-nl value)]
  =>
  (insert! (->Diagnoses "pt-0" "folate deficiency anemia")))

(defrule fe-deficiency-anemia
  [Diagnoses (= diagnosis "microcytic anemia")]
  [Lab (= test-name 'fe) (lab-low? fe-nl value)]
  [Lab (= test-name 'tibc) (lab-high? tibc-nl value)]
  [Lab (= test-name 'ferritin) (lab-low? ferritin-nl value)]
  =>
  (insert! (->Diagnoses "pt-0" "iron deficiency anemia: find cause")))

(defrule anemia-of-chronic-disease
  ;; Microcytic anemia with low Fe, normal or low TIBC, normal or
  ;; high ferritin
  [Diagnoses (= diagnosis "microcytic anemia")]
  [Lab (= test-name 'fe) (lab-low? fe-nl value)]
  [:or
   [Lab (= test-name 'tibc) (lab-low? tibc-nl value)]
   [Lab (= test-name 'tibc) (lab-normal? tibc-nl value)]]
  [:or
   [Lab (= test-name 'ferritin) (lab-high? ferritin-nl value)]
   [Lab (= test-name 'ferritin) (lab-normal? ferritin-nl value)]]
  =>
  (insert! (->Diagnoses "pt-0" "anemia of chronic disease")))

(defrule sideroblastic-anemia
  ;; Microcytic anemia with iron overload, siderocytes/sideroblasts
  [Diagnoses (= diagnosis "microcytic anemia")]
  [:or
   [Lab (= test-name 'fe) (lab-normal? fe-nl value)]
   [Lab (= test-name 'fe) (lab-high? fe-nl value)]]
  [:or
   [Lab (= test-name 'ferritin) (lab-high? ferritin-nl value)]
   [Lab (= test-name 'ferritin) (lab-normal? ferritin-nl value)]]
  [:or
   [Lab (= test-name 'sideroblasts)]
   [Lab (= test-name 'siderocytes)]]
  =>
  (insert! (->Diagnoses "pt-0" "sideroblastic anemia; find cause")))

(defrule thalassemias
  ;; Microcytic anemia with signs of α or β thalassemia
  [Diagnoses (= diagnosis "microcytic anemia")]
  [:or
   [Lab (= test-name 'fe) (lab-normal? fe-nl value)]
   [Lab (= test-name 'fe) (lab-high? fe-nl value)]]
  [:or
   [Lab (= test-name 'ferritin) (lab-high? ferritin-nl value)]
   [Lab (= test-name 'ferritin) (lab-normal? ferritin-nl value)]]
  [:or
   [Lab (= test-name 'teardrops)]
   [Lab (= test-name 'targetcells)]
   [Lab (= test-name 'splenomegaly)]]
  =>
  (insert! (->Diagnoses "pt-0" "α or β thalassemia; get Hgb electrophoresis")))

;; Queries

(def results (r/atom ""))
(def diagnoses (atom ""))
(def labs (atom ""))

(defquery get-patient-labs
  []
  [?pt-labs <- Lab])

(defquery get-patient-diagnoses
  []
  [?pt-dxs <- Diagnoses])

(defn show-all-labs!
  [session]
  (reset! labs "Based on the following lab results:\n")
  (doseq [{{test-name :test-name value :value} :?pt-labs} (query session get-patient-labs)]
    (swap! labs concat (str "   " test-name ": " value "\n"))
    (println (str "..." test-name ": " value)))
  (println "Final labs string is " @labs)
session)

(defn show-diagnoses!
  [session]
  (reset! diagnoses "Diagnoses:\n")
  (doseq [{{pt-name :pt-name diagnosis :diagnosis} :?pt-dxs}
          (query session get-patient-diagnoses)]
    (swap! diagnoses concat (str "   " diagnosis "\n"))
    (println (str "   " diagnosis)))
  (swap! diagnoses concat "\n")
  (println "Final diagnoses string is " @diagnoses)
  session)

(defsession example-session 'koantify.rules :cache false)

(def session-atom (atom example-session))
(def session-atom-empty (atom example-session))

(defn clear-session []
  (clear-ns-productions!)
  (reset! session-atom @session-atom-empty))

(defn insert-fact
  [lab-name lab-value]
  (reset! session-atom
          (insert @session-atom
                  (->Lab (symbol lab-name) lab-value "" "pt-0"))))

(defn show-results []
  (reset! session-atom (fire-rules @session-atom))
  (println "-------------Results-----------------")
  (println "Diagnoses:")
  (reset! session-atom (show-diagnoses! @session-atom))
  (println "Based on the following labs:")
  (reset! session-atom (show-all-labs! @session-atom)))




